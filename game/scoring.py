from collections import defaultdict


def score_hands(game):
    hand_scores, msg = get_hand_scores(game)
    msg += add_scores(game, hand_scores)

    return msg


def get_hand_scores(game):
    high_taker, low_taker, jack_taker = get_high_low_jack_takers(game)
    game_taker = get_game_point_taker(game)

    hand_scores = []
    hand_scores[high_taker] += 1
    hand_scores[low_taker] += 1

    # Only give the jack and game points if they were earned this hand
    if jack_taker > -1:
        hand_scores[jack_taker] += 1
    if game_taker > -1:
        hand_scores[game_taker] += 1

    msg = get_scored_hands_log_msg(high_taker, low_taker, jack_taker, game_taker)

    return hand_scores, msg


def get_scored_hands_log_msg(high_taker, low_taker, jack_taker, game_taker):
    msg = '<p><b>Player {}</b> took high.</p>'.format(high_taker + 1)
    msg += '<p><b>Player {}</b> took low.</p>'.format(low_taker + 1)

    # Only include jack and game point takers if they exist
    if jack_taker > -1:
        msg += '<p><b>Player {}</b> took jack.</p>'.format(jack_taker + 1)
    if game_taker > -1:
        msg += '<p><b>Player {}</b> took game.</p>'.format(game_taker + 1)
    else:
        msg += '<p>Players tied for game.</p>'

    return msg


def get_high_low_jack_takers(game):
    high_trump = 1
    high_taker = -1
    low_trump = 15
    low_taker = -1
    jack_taker = -1

    for player in range(game['num_players']):
        for card in game['tricks'][player]['cards']:
            if card['suit'] == game['trump']:
                if card['value'] > high_trump:
                    high_trump = card['value']
                    high_taker = player

                if card['value'] < low_trump:
                    low_trump = card['value']
                    low_taker = player

                if card['value'] == 11:
                    jack_taker = player

    return high_taker, low_taker, jack_taker


def get_game_point_taker(game):
    game_taker = -1
    game_scores = get_game_point_scores(game)

    max_pips = 0
    for player in range(game['num_players']):
        if game_scores[player] > max_pips:
            game_taker = player
            max_pips = game_scores[player]
        # Tie: set taker to -1
        elif game_scores[player] == max_pips:
            game_taker = -1

    return game_taker


def get_game_point_scores(game):
    game_scores = []

    card_values = {
        10: 10,
        11: 1,
        12: 2,
        13: 3,
        14: 4
    }
    card_values = defaultdict(lambda: 0, card_values)

    for player in range(game['num_player']):
        game_scores.append(0)

        for card in game['tricks'][player]['cards']:
            game_scores[player] += card_values[card['value']]

        print('>>> player {} game points: {}'.format(player, game_scores[player]))

    return game_scores


def add_scores(game, hand_scores):
    msg = ''

    for player in range(game['num_players']):
        if player == game['bidder']:
            if hand_scores[player] < game['bid']:
                hand_scores[game['bidder']] = -(game['bid'])
                msg += '<p><b>Player {0}</b> did not make their bid of {1} and loses {1} points.</p>'.format(player + 1, game['bid'])
            else:
                msg += '<p><b>Player {}</b> made their bid of {} and gets {} points.</p>'.format(player + 1, game['bid'], hand_scores[player])
        elif hand_scores[player] > 0:
            msg += '<p><b>Player {}</b> gets {} points.</p>'.format(player + 1, hand_scores[player])

    for player in range(game['num_players']):
        game['scores'][player] += hand_scores[player]

    return msg
